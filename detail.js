// define(function(require, exports, module) {
//
//     var ThreadShowWidget = require('../thread/thread-show.js');
//
//     exports.run = function() {
//
//         var threadShowWidget = new ThreadShowWidget({
//             element: '#detail-content',
//         });
//
//         threadShowWidget.element.on('click', '.js-asset-like', function() {
//             var $self = $(this);
//             if ($self.hasClass('color-primary')) {
//                 $.post($self.data('cancelLikeUrl'), function(asset) {
//                     $('.asset-content').find('.js-like-num').html(asset.upsNum);
//                 }).always(function(){
//                     $self.removeClass('color-primary');
//                     $self .closest('.icon-favour').removeClass('active');
//                 });
//             } else {
//                 $.post($self.data('likeUrl'), function(asset) {
//                     $('.asset-content').find('.js-like-num').html(asset.upsNum);
//                 }).always(function(){
//                     $self.addClass('color-primary');
//                     $self.closest('.icon-favour').addClass('active');
//                 });
//
//             }
//         });
//
//     };
//
// });

define(function(require, exports, module) {
  var videojs = require('video-js');

  var ThreadShowWidget = require('../thread/thread-show.js');

  exports.run = function() {

    var threadShowWidget = new ThreadShowWidget({
      element: '#content-container',
    });

    threadShowWidget.element.on('click', '#sendZipCode, #confirmBindQrCode', function() {
      var asset_id = $(this).attr('data');
      $.post('/asset/' + asset_id + '/code', function(data) {
        if (data.ret != 1) {
          alert('请等待确认绑定公众号');
          return false
        }
        alert(data.info);
        location.href = location.href + '?zipCode=' + data.data + '#showZipCode';
        //todo: false event
        //alert('提取密码已发送至公众号【VRstarter】, 请查收');
      });
    });

    threadShowWidget.element.on('click', '#bindQrCode', function() {
      $(".wechat-modal").show();
    });

    threadShowWidget.element.on('click', '#cancelBindQrCode', function() {
      $(".wechat-modal").hide();
    });

    threadShowWidget.element.on('click', '.js-asset-like', function() {
      var $self = $(this);
      if ($self.hasClass('color-primary')) {
        $.post($self.data('cancelLikeUrl'), function(asset) {
          $('.asset-content').find('.js-like-num').html(asset.upsNum);
        }).always(function() {
          $self.removeClass('color-primary');
          $self.html('<button class="status">收  藏</button>');
        });
      } else {
        $.post($self.data('likeUrl'), function(asset) {
          $('.asset-content').find('.js-like-num').html(asset.upsNum);
        }).always(function() {
          $self.addClass('color-primary');
          $self.html('<button class="status active">已收藏</button>');
        });

      }
    });

  };



  //vidio-swiper

  var swiper_slide = $('.asset-gallery-thumbs .swiper-slide');
  var swiper_top_slide = $('.asset-swiper-container1 .swiper-slide')
  var video_num = [];
  var video_flag = 'assetvideo' + video_num;
  var video_play_flag = true;

  for (let i = 0; i < swiper_slide.length; i++) {
    video_num.push(i);
  }
  swiper_slide.eq(0).addClass('active-nav');
  // 如果视频在第一个素材，则自动播放
  if ($('#assetvideo-0').length > 0) {
    var myplayer = videojs('assetvideo-0');
    myplayer.play();
  }

  //轮播图对象
  var viewSwiper = new Swiper('.asset-swiper-container1', {
    onSlideChangeStart: function() {
      updateNavPosition();
    }
  })

  //缩略图对象
  var previewSwiper = new Swiper('.asset-gallery-thumbs', {
    visibilityFullFit: true,
    slidesPerView: 4,
    onlyExternal: true,
    nextButton: '.swiper-sub-next',
    prevButton: '.swiper-sub-prev',
    onSlideClick: function() {
      video_play_flag = true;
      viewSwiper.swipeTo(previewSwiper.clickedSlideIndex);
      swiperPlay(viewSwiper.activeIndex);
      $('#assetvideo-'+viewSwiper.activeIndex).removeClass('pause');
    }
  })
  //轮播大图向后轮播按钮，到结尾跳转第一个
  $('.asset-swiper-button-prev').on('click', function(e) {
    video_play_flag = true;
    e.preventDefault();
    if (viewSwiper.activeIndex != 0) {
      viewSwiper.swipePrev();
    }
    swiperPlay(viewSwiper.activeIndex);
    $('#assetvideo-'+viewSwiper.activeIndex).removeClass('pause');
  })


  //轮播大图向前轮播按钮
  $('.asset-swiper-button-next').on('click', function(e) {
    video_play_flag = true;
    e.preventDefault();

    if (viewSwiper.activeIndex == viewSwiper.slides.length - 1) {
      viewSwiper.swipeTo(0, 500);
      swiperPlay(0);
    } else {
      viewSwiper.swipeNext();
      swiperPlay(viewSwiper.activeIndex);
      $('#assetvideo-'+viewSwiper.activeIndex).removeClass('pause');
    }
  })
  //缩略图向前轮播
  $('.swiper-sub-next').on('click', function(e) {
    e.preventDefault();
    if (viewSwiper.activeIndex == previewSwiper.slides.length - 1) {
      previewSwiper.swipeTo(0);
      viewSwiper.swipeTo(0);
    } else {
      previewSwiper.swipeNext();
      viewSwiper.swipeNext();
    }
    swiperPlay(viewSwiper.activeIndex);
    $('#assetvideo-'+viewSwiper.activeIndex).removeClass('pause');
  })

  //缩略图向后轮播
  $('.swiper-sub-prev').on('click', function(e) {
    e.preventDefault();
    if (viewSwiper.activeIndex != 0) {
      viewSwiper.swipePrev();
    }
    swiperPlay(viewSwiper.activeIndex);
    $('#assetvideo-'+viewSwiper.activeIndex).removeClass('pause');
  })

  // 点击屏幕切换暂停与播放
  for(var j = 0;j<swiper_top_slide.length;j++){
      $('.asset-swiper-container1 .swiper-slide').eq(j).on('click',function(e){
          if($(this).children('.asset-video').attr('id')){
              var pauseFlag = $(this).children('.asset-video');
              var pauseNum = pauseFlag.attr('id').split('-');
             if(pauseFlag.hasClass('pause')){
                 swiperPlay(pauseNum[1]);
                 pauseFlag.removeClass('pause')
             }else{
                 pausePlay();//暂停
                 pauseFlag.addClass('pause');
             }
         }else{
             console.log("no video!");
         }
      })
  }

  //播放当前视频
  function swiperPlay(arr) {
    for (var i in video_num) {
      if ($('#assetvideo-' + video_num[i] + '_html5_api').length > 0) {
        var oldplayer = videojs('assetvideo-' + video_num[i] + '_html5_api');
        oldplayer.pause();
      }
    }
    if ($('#assetvideo-' + arr).length > 0) {
      var myplayer = videojs('assetvideo-' + arr);
      myplayer.play();
    }
  }

  //暂停所有视频播放
  function pausePlay(){
      for (var i in video_num) {
        if ($('#assetvideo-' + video_num[i] + '_html5_api').length > 0) {
          var oldplayer = videojs('assetvideo-' + video_num[i] + '_html5_api');
          oldplayer.pause();
        }
      }
  }

  function updateNavPosition() {
    $('.asset-gallery-thumbs .active-nav').removeClass('active-nav');
    var activeNav = $('.asset-gallery-thumbs .swiper-slide').eq(viewSwiper.activeIndex).addClass('active-nav');
    if (!activeNav.hasClass('swiper-slide-visible')) {
      if (activeNav.index() > previewSwiper.activeIndex) {
        var thumbsPerNav = Math.floor(previewSwiper.width / activeNav.width()) - 1;
        previewSwiper.swipeTo(activeNav.index() - thumbsPerNav);
      } else {
        previewSwiper.swipeTo(activeNav.index());
      }
    }
  }

});
